Source: trapperkeeper-status-clojure
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Apollon Oikonomopoulos <apoikos@debian.org>
Build-Depends:
 debhelper (>= 10),
 javahelper (>= 0.32),
 maven-repo-helper (>= 1.7),
 clojure (>= 1.8),
 libclj-time-clojure (>= 0.11.0),
 libring-core-clojure (>= 1.4.0),
 libcommons-codec-java (>= 1.10),
 libtools-macro-clojure (>= 0.1.5),
 libtools-logging-clojure (>= 0.2.3),
 libtools-reader-clojure (>= 1.0.0~alpha1),
 libtools-trace-clojure (>= 0.7.9),
 libslf4j-java (>= 1.7.13),
 libcheshire-clojure (>= 5.6.1),
 libprismatic-schema-clojure (>= 1.1.1),
 libring-defaults-clojure (>= 0.2.0),
 libslingshot-clojure (>= 0.12.2),
 libversioneer-clojure,
 libjava-jmx-clojure (>= 0.3.1),
 libkitchensink-clojure,
 libtrapperkeeper-clojure,
 libtrapperkeeper-scheduler-clojure (>= 0.1.0),
 libpuppetlabs-ring-middleware-clojure (>= 1.0.0),
 libpuppetlabs-i18n-clojure,
 libpuppetlabs-http-client-clojure,
 libcomidi-clojure (>= 0.3.1),
 libtext-markdown-perl | markdown,
 default-jdk
Standards-Version: 4.0.0
Vcs-Git: https://salsa.debian.org/java-team/trapperkeeper-status-clojure.git
Vcs-Browser: https://salsa.debian.org/java-team/trapperkeeper-status-clojure
Homepage: https://github.com/puppetlabs/trapperkeeper-status

Package: libtrapperkeeper-status-clojure
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends}
Recommends: ${java:Recommends}
Description: status monitoring for trapperkeeper services
 The Trapperkeeper Status Service is a Trapperkeeper service that provides a
 web endpoint for getting status information for a Trapperkeeper-based
 application.
 .
 Other Trapperkeeper services may register a status callback function with
 the Status Service, returning any kind of status information that is relevant
 to the consuming service. The Status Service will make this information
 available via HTTP, in a consistent, consolidated format. This makes it
 possible for users to automate monitoring and other tasks around the system.
